package Decorator;

/**
 * Base case class for Decorator.Decorator pattern
 */
public class BaseMovie implements MovieInterface {

    int yearMade;

    BaseMovie(int yearMade) {
        this.yearMade = yearMade;
    }

    @Override
    public String playMovie() {
        return "Hello I am playing this movie in text";
    }

    @Override
    public int getYearMade() {
        // Apparently all movies were made in 1997
        return yearMade;
    }

    public static void main(String[] args) {

        BaseMovie theMatrix = new BaseMovie(1999);
        theMatrix.playMovie();
        theMatrix.getYearMade();
    }
}
