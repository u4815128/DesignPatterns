package Decorator;

/**
 * Interface for Observer pattern
 */
public interface MovieInterface {

    public String playMovie();
    public int getYearMade();
}
