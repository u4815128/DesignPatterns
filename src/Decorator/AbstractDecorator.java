package Decorator;

/**
 * Abstract Decorator.Decorator class for Observer pattern
 *
 *
 */
public abstract class AbstractDecorator implements MovieInterface {

    MovieInterface movieToBeDecorated;

    AbstractDecorator(MovieInterface movieToBeDecorated) {
        this.movieToBeDecorated = movieToBeDecorated;
    }

    public String playMovie() {
        return "Hello I am playing the movie in text";
    }



}
